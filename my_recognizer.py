import warnings
import sys
from asl_data import SinglesData


def recognize(models: dict, test_set: SinglesData):
    """ Recognize test word sequences from word models set

   :param models: dict of trained models
       {'SOMEWORD': GaussianHMM model object, 'SOMEOTHERWORD': GaussianHMM model object, ...}
   :param test_set: SinglesData object
   :return: (list, list)  as probabilities, guesses
       both lists are ordered by the test set word_id
       probabilities is a list of dictionaries where each key a word and value is Log Liklihood
           [{SOMEWORD': LogLvalue, 'SOMEOTHERWORD' LogLvalue, ... },
            {SOMEWORD': LogLvalue, 'SOMEOTHERWORD' LogLvalue, ... },
            ]
       guesses is a list of the best guess words ordered by the test set word_id
           ['WORDGUESS0', 'WORDGUESS1', 'WORDGUESS2',...]
   """
    warnings.filterwarnings("ignore", category=DeprecationWarning)
    probabilities = []
    guesses = []
    # Implemented the recognizer

    for i in range(test_set.num_items):
        X, lengths = test_set.get_item_Xlengths(i)
        probability = {}
        max_log = - sys.maxsize
        best_guess='Not Found'
        for name in models:
            model = models[name]
            probability[name] = None
            try:
                log_l = model.score(X,lengths)
                probability[name] = log_l
            except:
                pass
            if log_l>max_log:
                max_log = log_l
                best_guess = name
        guesses.append(best_guess)
        probabilities.append(probability)
    return probabilities, guesses
