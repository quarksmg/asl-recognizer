import math
import statistics
import warnings

import numpy as np
from hmmlearn.hmm import GaussianHMM
import sys
from sklearn.model_selection import KFold
from asl_utils import combine_sequences


class ModelSelector(object):
    '''
    base class for model selection (strategy design pattern)
    '''

    def __init__(self, all_word_sequences: dict, all_word_Xlengths: dict, this_word: str,
                 n_constant=3,
                 min_n_components=2, max_n_components=10,
                 random_state=14, verbose=False):
        self.words = all_word_sequences
        self.hwords = all_word_Xlengths
        self.sequences = all_word_sequences[this_word]
        self.X, self.lengths = all_word_Xlengths[this_word]
        self.this_word = this_word
        self.n_constant = n_constant
        self.min_n_components = min_n_components
        self.max_n_components = max_n_components
        self.random_state = random_state
        self.verbose = verbose

    def select(self):
        raise NotImplementedError

    def base_model(self, num_states):
        # with warnings.catch_warnings():
        warnings.filterwarnings("ignore", category=DeprecationWarning)
        # warnings.filterwarnings("ignore", category=RuntimeWarning)
        try:
            hmm_model = GaussianHMM(n_components=num_states, covariance_type="diag", n_iter=1000,
                                    random_state=self.random_state, verbose=False).fit(self.X, self.lengths)
            if self.verbose:
                print("model created for {} with {} states".format(self.this_word, num_states))
            return hmm_model
        except:
            if self.verbose:
                print("failure on {} with {} states".format(self.this_word, num_states))
            return None


class SelectorConstant(ModelSelector):
    """ select the model with value self.n_constant

    """

    def select(self):
        """ select based on n_constant value

        :return: GaussianHMM object
        """
        best_num_components = self.n_constant
        return self.base_model(best_num_components)


class SelectorBIC(ModelSelector):
    """ select the model with the lowest Bayesian Information Criterion(BIC) score

    http://www2.imm.dtu.dk/courses/02433/doc/ch6_slides.pdf
    Bayesian information criteria: BIC = -2 * logL + p * logN
    """

    def select(self):
        """ select the best model for self.this_word based on
        BIC score for n between self.min_n_components and self.max_n_components

        :return: GaussianHMM object
        """
        warnings.filterwarnings("ignore", category=DeprecationWarning)

        # implemented model selection based on BIC scores

        min_bic = sys.maxsize
        ret = None
        # This is taken from https://discussions.udacity.com/t/number-of-data-points-bic-calculation/235294/6
        datapoints = len(self.X)
        feature_number = len(self.X[0])
        for num_states in range( self.min_n_components,self.max_n_components+1):
            try:
                model = self.base_model(num_states)
            except:
                continue
            # Calculate the BIC
            # This is taken from https://ai-nd.slack.com/files/ylu/F4S90AJFR/number_of_parameters_in_bic.txt
            p = (num_states**2) + (2*num_states*feature_number) - 1
            logL = 0
            try:
                logL = model.score(self.X, self.lengths)
            except:
                continue
            bic = -2*logL + p*math.log(datapoints)
            if bic < min_bic:
                min_bic = bic
                ret = model
        return ret


class SelectorDIC(ModelSelector):
    ''' select best model based on Discriminative Information Criterion

    Biem, Alain. "A model selection criterion for classification: Application to hmm topology optimization."
    Document Analysis and Recognition, 2003. Proceedings. Seventh International Conference on. IEEE, 2003.
    http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.58.6208&rep=rep1&type=pdf
    https://pdfs.semanticscholar.org/ed3d/7c4a5f607201f3848d4c02dd9ba17c791fc2.pdf
    DIC = log(P(X(i)) - 1/(M-1)SUM(log(P(X(all but i))
    '''

    def select(self):
        warnings.filterwarnings("ignore", category=DeprecationWarning)

        # TODO implement model selection based on DIC scores
        ret = None
        max_dic = -sys.maxsize
        other_words = [word for word in self.words if word != self.this_word]
        for num_states in range(self.min_n_components,self.max_n_components+1):
            model=None
            try:
                model= self.base_model(num_states)
                logL= model.score(self.X,self.lengths)
                othersLogLs = []
                for other in other_words:
                    X, lengths = self.hwords[other]
                    otherLogL = model.score(X,lengths)
                    othersLogLs.append(otherLogL)
                dic = logL - np.mean(othersLogLs)
            except:
                continue
            dic = 0
            if dic > max_dic:
                max_dic=dic
                ret=model
        return ret


class SelectorCV(ModelSelector):
    ''' select best model based on average log Likelihood of cross-validation folds

    '''

    def select(self):
        warnings.filterwarnings("ignore", category=DeprecationWarning)

        # implemented model selection using CV
        ret = None
        max_avg = -sys.maxsize
        for num_states in range(self.min_n_components,self.max_n_components+1):
            logLs = []
            split_method = KFold(n_splits=2)
            model_has_error = False
            # during full training,
            # the word write is returning only one sequence these has to be handled properly
            if len(self.sequences) < 2:
                split = [(0,0)]
            else:
                split = [ (cv_train_idx, cv_test_idx) for cv_train_idx, cv_test_idx in split_method.split(self.sequences)]
            for cv_train_idx, cv_test_idx in split:
                try:
                    model = self.createModelWOFit(num_states)
                    X, lengths = combine_sequences(cv_train_idx,self.sequences)
                    model.fit(X, lengths)
                    X, lengths = combine_sequences(cv_test_idx, self.sequences)
                    logL = model.score(X, lengths)
                    logLs.append(logL)
                except:
                    model_has_error=True
                    break
            if model_has_error:
                continue
            avg = np.mean(logLs)
            if avg > max_avg:
                max_avg=avg
                ret=model
        return ret
    def createModelWOFit(self,num_states):
        try:
            hmm_model = GaussianHMM(n_components=num_states, covariance_type="diag", n_iter=1000,
                                    random_state=self.random_state, verbose=False)
            if self.verbose:
                print("model created for {} with {} states".format(self.this_word, num_states))
            return hmm_model
        except:
            if self.verbose:
                print("failure on {} with {} states".format(self.this_word, num_states))
            return None